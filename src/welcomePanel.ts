/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *--------------------------------------------------------------------------------------------*/
'use strict';
import * as path from 'path';
import * as vscode from 'vscode';

export class WelcomePanel {
	public static currentPanel?: WelcomePanel;
	public static readonly viewType = 'vscode-remote';
	private readonly _panel: vscode.WebviewPanel;
	private readonly _extensionPath: string;

	private _disposables: vscode.Disposable[] = [];

	public static createOrShow(extensionPath: string) {
		const column = vscode.window.activeTextEditor
			? vscode.window.activeTextEditor.viewColumn
			: undefined;

		if (WelcomePanel.currentPanel) {
			WelcomePanel.currentPanel._panel.reveal(column, true);
		} else {
			WelcomePanel.currentPanel = new WelcomePanel(extensionPath, column);
		}
	}

	private constructor(extensionPath: string, column?: vscode.ViewColumn) {
		this._extensionPath = extensionPath;

		this._panel = vscode.window.createWebviewPanel(WelcomePanel.viewType, 'VS Code Remote: Quick Starts', column || vscode.ViewColumn.One, {
			enableScripts: true,
			enableCommandUris: true,
			localResourceRoots: [
				vscode.Uri.file(path.join(this._extensionPath, 'out'))
			]
		});

		this._panel.webview.html = this.getHtmlForWebview();
		this._panel.onDidDispose(() => this.dispose(), null, this._disposables);
	}

	private getHtmlForWebview() {
		const scriptPathOnDisk = vscode.Uri.file(path.join(this._extensionPath, 'out', 'index.js'));
		const scriptUri = scriptPathOnDisk.with({ scheme: 'vscode-resource' });
		const nonce = getNonce();

		return `<!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src vscode-resource: https:; script-src 'nonce-${nonce}'; style-src vscode-resource: 'unsafe-inline' http: https: data:;">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<base href="${scriptUri}">
			</head>
			<body class="${process.platform} welcomePageContainer">
				<div id="main" class="welcomePage">
					<h1>Visual Studio Code Remote Development</h1>
					<div id="common"></div>
					<p class="subtitle detail">Quick Starts</p>
					<div id="ssh"></div>
					<div id="containers"></div>
					<div id="wsl"></div>
					<h2>Questions, Comments, Feedback</h2>
					<div id="feedback"></div>
					</div>
				</div>
				<script nonce="${nonce}" src="${scriptUri}"></script>
			</body>
			</html>`;
	}

	dispose() {
		WelcomePanel.currentPanel = undefined;

		this._panel.dispose();

		while (this._disposables.length) {
			const x = this._disposables.pop();
			if (x) {
				x.dispose();
			}
		}
	}
}

function getNonce() {
	let text = '';
	const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	for (let i = 0; i < 32; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
}