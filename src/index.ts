
import "./index.css"

import md from "./markdown"


const isWindows = document.body.classList.contains("win32")

document.getElementById("common").innerHTML = md.render(
    `
Thanks for downloading the Visual Studio Code Remote Development extension pack! The included SSH, Containers, and WSL extensions enable VS Code to provide a **local-quality development experience** including full IntelliSense (completions), debugging, and more **regardless of where your code is located or hosted**.

You can get an idea of what you can do with the different extensions by clicking on the remote development status bar item in the lower-left.

![Remote Development status bar item](https://microsoft.github.io/vscode-remote-release/images/remote-dev-status-bar.png)

This will give you a scoped list of available commands you can pick from to get started. Pick one of the quick starts below to learn more!
    `
)

document.getElementById("feedback").innerHTML = md.render(
    `
Have a question or feedback? There are many ways to interact with us.

- See the [documentation](https://aka.ms/vscode-remote) or the [troubleshooting guide](https://aka.ms/vscode-remote-release/troubleshooting).
- [Up-vote a feature or request a new one](https://aka.ms/vscode-remote-release/feature-requests), search [existing issues](https://aka.ms/vscode-remote-release/issues), or [report a problem](https://aka.ms/vscode-remote-release/issues/new)
- Contribute a [development container definition](https://aka.ms/vscode-dev-containers) for others to use.
- Contribute to [our documentation](https://github.com/Microsoft/vscode-docs) or [VS Code itself](https://github.com/Microsoft/vscode).
- ...and more. See our [CONTRIBUTING](https://aka.ms/vscode-remote/contributing) guide for details.

Or connect with the community...

[![Twitter](https://microsoft.github.io/vscode-remote-release/images/Twitter_Social_Icon_24x24.png)](https://aka.ms/vscode-remote-release/twitter) [![Stack Overflow](https://microsoft.github.io/vscode-remote-release/images/so-image-24x24.png)](https://stackoverflow.com/questions/tagged/vscode) [![VS Code Dev Community Slack](https://microsoft.github.io/vscode-remote-release/images/Slack_Mark-24x24.png)](https://aka.ms/vscode-dev-community) [![VS CodeGitter](https://microsoft.github.io/vscode-remote-release/images/gitter-icon-24x24.png)](https://gitter.im/Microsoft/vscode)
    `
)

document.getElementById("ssh").innerHTML = md.render(
    `
## SSH

Given nearly every desktop and server operating system has an optional SSH server that can be configured, you'll be able to use the SSH extension in **many different scenarios**. Once connected to a host, you can interact with files, folders, and workspaces sitting **anywhere on the remote filesystem** and install extensions that will be available from any machine you use to connect to it.

<details><summary>Click to expand the quick start</summary>

1. Install a [supported SSH client](https://aka.ms/vscode-remote/ssh/supported-clients).
${isWindows ? "    > **Note:** PuTTY is not supported on Windows since an OpenSSH compatible `ssh` command must be in the path." : ""}

2. **Configure key based authentication** on the host you plan to use. If you are new to SSH or are running into trouble, see [here for additional information](https://aka.ms/vscode-remote/ssh/key-based-auth) on setting this up.

3. Run [Remote-SSH: New Window...](command:opensshremotes.openEmptyWindow) from the command palette and enter the host and user name in the input box as follows: \`user@hostname\` (on Windows \`user@domain@hostname\` is also supported).

    ![Input user@hostname](https://microsoft.github.io/vscode-remote-release/images/ssh-user@box.png)

4. After a moment, VS Code will connect to the SSH server and set itself up. VS Code will keep you up to date using a progress notification.

5. After you are connected, you'll see a empty window and you can then open a folder or workspace on the remote machine using [File > Open...](command:workbench.action.files.openFileFolder) or [File > Open Workspace...](command:workbench.action.openWorkspace).

6. After a moment the folder or workspace you selected will open. **Install any extensions** you want to use on this host from the extension panel to customize the environment to your needs.

You can now use VS Code as you would normally! See the [extension documentation](https://aka.ms/vscode-remote/ssh) to learn more!
</details>
`
)

const isLinux = document.body.classList.contains("linux")

document.getElementById("containers").innerHTML = md.render(
    `
## Containers

The Containers extension can adapt to a variety of different container-based workflows. The extension supports two primary operating models. One is to use a container as your [full-time development environment](https://aka.ms/vscode-remote/containers/folder-setup) while the other is to [attach to a running container](https://aka.ms/vscode-remote/containers/attach) for targeted use.

Click below to expand a quick start:
<details><summary>Try out a sample container</summary>

You can quickly take a tour of what VS Code Remote Development can do or try out a new technology stack without installing anything locally in a few easy steps:

${
    isLinux
        ? "1. Install [Docker CE/EE for Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/). Follow the **official** Docker install instructions such as using the [convenience script](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-convenience-script) (rather than the unoffical Ubuntu snap package) and add your user to the `docker` group after installation as follows: `sudo usermod -aG docker $USER`."
        : "1. Install [Docker Desktop for Mac/Windows](https://www.docker.com/products/docker-desktop)."
    }

2. Clone the [vscode-dev-containers repository](http://aka.ms/vscode-dev-containers).

    \`\`\`bash
    git clone https://github.com/Microsoft/vscode-dev-containers
    \`\`\`

	${isWindows ? "    > **Note:** We recommend **disabling automatic line ending conversion** for Git on the **Windows side** given Linux (and thus WSL and Containers) use different line endings. Run this command to update the setting before cloning the repository: `git config --global core.autocrlf false`" : ""}

3. Run the [Remote-Containers: Open Folder in Container...](command:openindocker.openFolder) command from the command palette.

5. Select one of the folders in the \`containers\` directory of the cloned copy of the vscode-dev-containers repository that interests you.

6. The window will then reload, but since the container does not exist yet, VS Code will provision one. This can take some time, so a progress notification will provide status updates.

	![Dev Container Progress Notification](https://microsoft.github.io/vscode-remote-release/images/dev-container-progress.png)

After it's done, VS Code will automatically connect to the container. The local filesystem will be automatically mapped into the container so you can interact with it just as you would if it was running locally!

</details>

<details><summary>Open an existing project folder in a container</summary>

In this quick start, we will cover how to set up an existing project folder to use a container as your full-time development environment.

${
    isLinux
        ? "1. Install [Docker CE/EE for Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/). Follow the **official** Docker install instructions such as using the [convenience script](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-convenience-script) (rather than the unoffical Ubuntu snap package) and add your user to the `docker` group after installation as follows: `sudo usermod -aG docker $USER`."
        : "1. Install [Docker Desktop for Mac/Windows](https://www.docker.com/products/docker-desktop)."
    }

2. Start VS Code and run the [Remote-Containers: Open Folder in Container...](command:openindocker.openFolder) command from the command pallette and select the folder you want to open.
${
    isWindows
        ? "    > **Note:** We recommend **disabling automatic line ending conversion** for Git on the **Windows side** given Linux (and thus WSL and Containers) use different line endings. Run this command to update the setting before cloning a repository: `git config --global core.autocrlf false`"
        : ""
    }

3. You'll then be asked to pick a **dev container definition** to use as a starting point for your container. If there is a [Dockerfile](https://docs.docker.com/engine/reference/builder/) or [Docker Compose file](https://docs.docker.com/compose/compose-file/#compose-file-structure-and-examples) in the folder, you also have the option to use it instead.

    ![Dev Container Progress Notification](https://microsoft.github.io/vscode-remote-release/images/select-dev-container-def.png)

    > **Note:** VS Code Remote - Containers does not currently support Alpine or Windows based containers.

    All of these come from the [vscode-dev-containers repository](http://aka.ms/vscode-dev-containers) if you'd like to take a look at their contents before picking one.

6. Once you pick an option, the needed configuration files like \`devcontainer.json\` will be added to the folder and the window will then reload. Since the container does not exist yet, VS Code will provision one. This can take some time, so a progress notification will provide status updates.

    ![Dev Container Progress Notification](https://microsoft.github.io/vscode-remote-release/images/dev-container-progress.png)

After it's done, VS Code will automatically connect to the container. The local filesystem will be automatically mapped into the container so you can interact with it just as you would if it was running locally!

The intent of the \`.devcontainer.json\` or \`.devcontainer/devcontainer.json\` file added to the folder is conceptually similar to VS Code's \`launch.json\` for debugging, but focused on launching (or attaching to) your development container instead. Once in your project, you can update the file to [alter your configuration](https://aka.ms/vscode-remote/containers/folder-setup) to install additional tools like Git in the container, automatically install extensions, expose additional ports, set runtime arguments, reuse or [extend your existing Docker Compose setup](https://aka.ms/vscode-remote/containers/docker-compose/extend), and more. The [vscode-dev-containers repository](http://aka.ms/vscode-dev-containers) can also be a good resource for examples of adapting \`devcontainer.json\` to different scenarios.

See the [extension documentation](https://aka.ms/vscode-remote/containers) to learn more!

</details>
`
)

if (isWindows) {

    document.getElementById("wsl").innerHTML = md.render(
        `
## Windows Subsystem for Linux (WSL)

The **[Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl)** allows you to take advantage of a Linux based tool-chain right from the comfort of Windows. Getting going with VS Code's new, **optimized support** for WSL is easy.

<details><summary>Click to expand the quick start</summary>

1. Install the **[Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10)** along with your preferred Linux distribution.

    > **Note:** WSL does have some [known limitations](https://aka.ms/vscode-remote/wsl/limitations) for certain types of development that can also affect your VS Code experience.

2. Open a **WSL terminal window** (using the start menu item or by typing \`wsl\` from the command prompt).

3. Navigate to a folder you'd like to open in VS Code (including, but not limited to, Windows filesystem mounts like \`/mnt/c\`)

4. Type **\`code .\`** in the terminal. When doing this for the first time you should see VS Code fetching components needed to run in WSL. This should only take short while, and is only needed once.

6. After a moment, a new VS Code window will appear and you'll see a notification letting you know VS Code is opening the folder in WSL.

   ![WSL Starting notification](https://microsoft.github.io/vscode-remote-release/images/wsl-starting-notification.png)

    VS Code will now continue to configure itself in WSL, and install any VS Code extensions you are running locally inside WSL to optimize performance. VS Code will keep you up to date as it makes progress.

7. Once finished, you now see a WSL indicator in the bottom left corner, and you'll be able to use VS Code as you would normally!

    ![WSL Status Bar Item](https://microsoft.github.io/vscode-remote-release/images/wsl-statusbar-indicator.png)

See the [extension documentation](https://aka.ms/vscode-remote/wsl) to learn more!

</details>
`
    )

}
