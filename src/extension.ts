/*---------------------------------------------------------------------------------------------
 *  Copyright (c) Microsoft Corporation. All rights reserved.
 *--------------------------------------------------------------------------------------------*/
'use strict';

import * as vscode from 'vscode';
import { WelcomePanel } from './welcomePanel';

export function activate(context: vscode.ExtensionContext) {
	let disposable = vscode.commands.registerCommand('vscode-remote-extensionpack.welcome', () => {
		WelcomePanel.createOrShow(context.extensionPath);
	});

	context.subscriptions.push(disposable);
}

export function deactivate() {
}