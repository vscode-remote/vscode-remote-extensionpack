
import MarkdownIt  from "markdown-it"

const md = MarkdownIt({
    breaks: true,
    html: true,
    linkify: true
})

export default md
